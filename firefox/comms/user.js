// SPDX-License-Identifier: CC0-1.0
user_pref("privacy.sanitize.sanitizeOnShutdown", true)
user_pref("privacy.clearOnShutdown.history", true)
user_pref("privacy.clearOnShutdown.downloads", true)
user_pref("privacy.clearOnShutdown.cookies", false)
user_pref("privacy.clearOnShutdown.sessions", false)
user_pref("privacy.clearOnShutdown.cache", false)
user_pref("privacy.clearOnShutdown.formdata", true)
user_pref("privacy.clearOnShutdown.offlineApps", false)
user_pref("privacy.clearOnShutdown.openWindows", true)
user_pref("browser.tabs.firefox-view", false);
