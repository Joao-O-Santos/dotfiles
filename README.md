# Dotfiles

These are the dotfiles for the programs I use.

Feel free to modify them and share their modifications in accordance
with the legalities stated below.


## Legalities

The files or sets of modifications, that I've authored, in this project,
if ever considered copyrightable, are hereby licensed under CC0-1.0 (see
`./LICENSES/CC0-1.0`). **However, you still need to comply with the
original licenses for the files others have authored (see table
below).** I also kindly ask you that you don't use any of these files to
do evil and/or to neglect doing good to others and/or yourself.

**WARNING: Some configuration files are the works of other authors and
are under different licenses.** In those cases the original licenses
must be obeyed. This mostly means that if you share those files you must
include the original author's copyright notice and the full text of the
license. The copyright notice is at the top of said files. The full text
for the license is in `./LICENSES/` and is named after the corresponding
spdx license identifier. Below is a table with the files and their
corresponding licenses, please refer to it if the file does not include
an spdx license identifier.

| File                          | SPDX-License-Identifier      |
|-------------------------------|------------------------------|
| `.gitignore`                  | CC0-1.0                      |
| `R/Rprofile`                  | GPL-2.0-only or GPL-3.0-only |
| `README.md`                   | CC0-1.0                      |
| `firefox/hardened/user.js`    | MIT                          |
| `firefox/unsafe/user.js`      | MIT                          |
| `firefox/unsafest/user.js`    | CC0-1.0                      |
| `firefox/pdfviewer/user.js`   | CC0-1.0                      |
| `firefox/profiles.ini`        | CC0-1.0                      |
| `git/config`                  | CC0-1.0                      |
| `i3/config`                   | BSD-3-Clause                 |
| `i3status/config`             | BSD-3-Clause                 |
| `link_configs.sh`             | CC0-1.0                      |
| `mimeapps.list`               | CC0-1.0                      |
| `nvim/init.vim`               | CC0-1.0                      |
| `pandoc/docx/word/styles.xml` | CC0-1.0                      |
| `playlist.m3u`                | CC0-1.0                      |
| `profile`                     | CC0-1.0                      |
| `scripts/backup.sh`           | CC0-1.0                      |
| `scripts/exclude`             | CC0-1.0                      |
| `shellrc`                     | CC0-1.0                      |
| `task/taskrc`                 | GPL-3.0-only                 |
| `xorg`                        | CC0-1.0                      |
