# SPDX-License-Identifier: CC0-1.0
ln -s $XDG_CONFIG_HOME/profile $HOME/.profile
ln -s $XDG_CONFIG_HOME/xorg/xinitrc $HOME/.xinitrc
ln -s $XDG_CONFIG_HOME/firefox/profiles.ini $HOME/.mozilla/firefox/profiles.ini 
ln -s $XDG_CONFIG_HOME/firefox/hardened/user.js $HOME/.mozilla/firefox/hardened/user.js
ln -s $XDG_CONFIG_HOME/firefox/unsafe/user.js $HOME/.mozilla/firefox/unsafe/user.js
ln -s $XDG_CONFIG_HOME/firefox/unsafest/user.js $HOME/.mozilla/firefox/unsafest/user.js
ln -s $XDG_CONFIG_HOME/firefox/pdfviewer/user.js $HOME/.mozilla/firefox/pdfviewer/user.js
ln -s $XDG_CONFIG_HOME/firefox/comms/user.js $HOME/.mozilla/firefox/comms/user.js
